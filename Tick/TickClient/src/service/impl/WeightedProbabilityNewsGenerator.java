package service.impl;

import dto.News;
import service.HeadlineGenerator;
import service.NewsGenerator;
import service.PriorityGenerator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class WeightedProbabilityNewsGenerator implements NewsGenerator {

    PriorityGenerator priorityGenerator;

    public News generator(int[] distributedProbabilities) {
        priorityGenerator = new WeightedPriorityGenerator();
        int priority = priorityGenerator.getPriority(distributedProbabilities);
        String headline = HeadlineGenerator.generator();
        News newsDTO = new News(headline, priority);
        System.out.println(newsDTO);
        return newsDTO;
    }
}
