package service.impl;

import service.PriorityGenerator;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class WeightedPriorityGenerator implements PriorityGenerator {

    @Override
    public int getPriority(int[] distributedProbabilities) {
        return getWeightedPriority(distributedProbabilities);
    }

    /**
     * @param distributedProbabilities : Receives a histogram of successive additive probability range.
     *                                 For probability between 0-9, the array would have values like
     *                                 {0,1,3,6,10,15,21,28,36,45}
     * The method gets random number between 0 and max distributed probability i.e. 45, then searches for the
     *                                 range that contains that number. Once the range is found the actual/original
     *                                 probability corresponding to that range is returned. For efficiency, I am
     *                                 using binary search instead of linear search as the @param distributedProbabilities
     *                                 is already sorted.
     * Example: We have 10 numbers from 0 to 9, and 0 has probability 10 times bigger than 9, so we can just get a normal
     *                                 random number from 0 to 45 (1+2+3+4+5+6+7+8+9) and then numbers from <0; 9>
     *                                     represent 0, numbers from <10; 18> represents 1 ... <45> represents 9
     * @return weighted probability
     */
    private int getWeightedPriority(int[] distributedProbabilities) {
        int maxPriority = distributedProbabilities.length;
        int sumOfPossiblePriorities = distributedProbabilities[maxPriority-1];
        int weightedPriorityValue = ThreadLocalRandom.current().nextInt(sumOfPossiblePriorities);
        int weightedPriorityIndex = Arrays.binarySearch(distributedProbabilities, weightedPriorityValue);
        return weightedPriorityIndex >= 0 ? weightedPriorityIndex : -weightedPriorityIndex - 1;
    }
}
