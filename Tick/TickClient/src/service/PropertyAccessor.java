package service;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyAccessor {

    private static final String PROPERTIES_FILE = "resources/config.properties";

    public static int getFrequencyPropertyValue(String propertyKey) throws IOException {
        FileInputStream input = new FileInputStream(PROPERTIES_FILE);
        Properties prop = new Properties();
        prop.load(input);
        int frequency = Integer.parseInt(prop.getProperty(propertyKey));
        System.out.println("Read property -> "+ propertyKey +" = "+frequency);
        return frequency;
    }
}
