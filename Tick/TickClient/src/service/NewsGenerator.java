package service;

import dto.News;

public interface NewsGenerator {

    News generator(int[] distributedProbabilities);
}