package service;

import java.util.stream.IntStream;

public class DistributedProbabilitiesGenerator {

    public static int[] getDistributedProbabilities(int maxValue) {
        int[] possiblePriorities = IntStream.range(0, maxValue + 1)
                .toArray();
        for (int index = 1; index < possiblePriorities.length; ++index) {
            possiblePriorities[index] += possiblePriorities[index - 1];
        }
        return possiblePriorities;
    }
}
