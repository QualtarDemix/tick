package service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class HeadlineGenerator {

    private static final List<String> WORDS = Arrays.asList("up", "down", "rise", "fall", "good", "bad",
            "success", "failure", "high", "low", "über", "unter");
    private static final int MAX_HEADLINE_SIZE = 5;
    private static final int MIN_HEADLINE_SIZE = 3;
    private static final String DELIMITER = " ";

    public static String generator() {
        int headlineSize = ThreadLocalRandom.current()
                .nextInt(MIN_HEADLINE_SIZE, MAX_HEADLINE_SIZE + 1);
        Collections.shuffle(WORDS);
        List<String> headlineList = WORDS.subList(0, headlineSize);
        return String.join(DELIMITER, headlineList);
    }
}
