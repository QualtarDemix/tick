package service;

public interface PriorityGenerator {

    int getPriority(int[] distributedProbabilities);
}
