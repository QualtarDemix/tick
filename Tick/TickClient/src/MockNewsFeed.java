import dto.News;
import service.DistributedProbabilitiesGenerator;
import service.NewsGenerator;
import service.PropertyAccessor;
import service.impl.WeightedProbabilityNewsGenerator;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public class MockNewsFeed
{
    private static final String HOST = "127.0.0.1";
    private static final int PORT = 5000;
    private static final String FREQUENCY_PROPERTY_KEY = "frequency";
    private static final int MILLI_SECONDS = 1000;
    private static final int MAX_PRIORITY = 9;

    /**
     * Calculating distributed probabilities before starting thread as its a static value and computing it
     * multiple times would be inefficient
     */
    private static final int[] distributedProbabilities = DistributedProbabilitiesGenerator
            .getDistributedProbabilities(MAX_PRIORITY);

    public static void main(String[] args) throws IOException, InterruptedException {
        ObjectOutputStream objectOutputStream = getObjectOutputStream();
        NewsGenerator newsGenerator = new WeightedProbabilityNewsGenerator();
        int frequency = PropertyAccessor.getFrequencyPropertyValue(FREQUENCY_PROPERTY_KEY);
        while(true) {
            News newsDTO = newsGenerator.generator(distributedProbabilities);
            objectOutputStream.writeObject(newsDTO);
            //Higher the frequency, lesser the delay between successive calls
            Thread.sleep(MILLI_SECONDS/frequency);
        }
    }

    private static ObjectOutputStream getObjectOutputStream() throws IOException {
        Socket socket = new Socket(HOST, PORT);
        //Connect to a running server on the HOST and the PORT
        OutputStream outputStream = socket.getOutputStream();
        System.out.println("Started connection @ Host = "+HOST+" and port = "+PORT);
        return new ObjectOutputStream(outputStream);
    }
}
