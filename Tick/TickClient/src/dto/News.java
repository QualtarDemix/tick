package dto;

import java.io.Serializable;

public class News implements Serializable {

    public String headLine;
    public Integer priority;

    public News(String headLine, Integer priority) {
        this.headLine = headLine;
        this.priority = priority;
    }

    public String getHeadLine() {
        return headLine;
    }

    public void setHeadLine(String headLine) {
        this.headLine = headLine;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "News { " +
                "headLine='" + headLine + '\'' +
                ", priority=" + priority +
                " }";
    }
}
