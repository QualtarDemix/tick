package service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DistributedProbabilitiesGeneratorTest {

    private static final int MAX_PRIORITY = 9;

    @Test
    public void getDistributedProbabilitiesForMaxPriority() {
        int[] actualValue = DistributedProbabilitiesGenerator.getDistributedProbabilities(MAX_PRIORITY);
        int[] expectedValue = {0,1,3,6,10,15,21,28,36,45};

        Assert.assertArrayEquals(actualValue, expectedValue);
    }

    @Test
    public void getEmptyArrayForNegativeValue() {
        int[] actualValue = DistributedProbabilitiesGenerator.getDistributedProbabilities(-1);

        Assert.assertEquals(0, actualValue.length);
    }
}