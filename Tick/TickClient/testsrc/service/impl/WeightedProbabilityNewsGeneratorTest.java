package service.impl;

import dto.News;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import service.DistributedProbabilitiesGenerator;
import service.NewsGenerator;

import static org.junit.Assert.*;

public class WeightedProbabilityNewsGeneratorTest {

    private static final int MAX_PRIORITY = 9;

    NewsGenerator newsGenerator;

    @Before
    public void setUp() {
        newsGenerator = new WeightedProbabilityNewsGenerator();
    }

    @Test
    public void generateNewsForDistributedProbabilities() {
        int[] distributedProbabilities = DistributedProbabilitiesGenerator.getDistributedProbabilities(MAX_PRIORITY);

        News actualNews = newsGenerator.generator(distributedProbabilities);
        String actualHeadLine = actualNews.getHeadLine();
        Integer actualPriority = actualNews.getPriority();

        Assert.assertNotNull(actualNews);
        Assert.assertNotEquals("",actualHeadLine);
        Assert.assertNotNull(actualPriority);
        Assert.assertTrue(actualPriority>=0);
        Assert.assertTrue(actualPriority<=9);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionForIncorrectDistributedProbabilities() {
        int[] probabilities = {5,4,3,2,1,0};

        newsGenerator.generator(probabilities);
    }
}