package service.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import service.DistributedProbabilitiesGenerator;
import service.PriorityGenerator;


public class WeightedPriorityGeneratorTest {

    private static final int MAX_PRIORITY = 9;

    PriorityGenerator priorityGenerator;

    @Before
    public void setUp() {
        priorityGenerator = new WeightedPriorityGenerator();
    }

    @Test
    public void getWeightedPriorityForLinearAdditiveArray() {
        int[] distributedProbabilities = DistributedProbabilitiesGenerator.getDistributedProbabilities(MAX_PRIORITY);

        int actualValue = priorityGenerator.getPriority(distributedProbabilities);

        Assert.assertTrue(actualValue>=0);
        Assert.assertTrue(actualValue<=9);
    }

    @Test
    public void getWeightedPriorityForLinearContinuousArray() {
        int[] probabilities = {0,1,2,3,4,5};

        int actualValue = priorityGenerator.getPriority(probabilities);

        Assert.assertTrue(actualValue>=0);
        Assert.assertTrue(actualValue<=5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionForArraysThatDoNotIncreaseContinuously() {
        int[] probabilities = {5,4,3,2,1,0};

        priorityGenerator.getPriority(probabilities);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionForEmptyArray() {
        int[] distributedProbabilities = new int[10];

        priorityGenerator.getPriority(distributedProbabilities);
    }
}