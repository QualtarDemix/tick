import dto.News;
import services.NewsService;
import services.impl.PositiveNewsService;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.time.Instant;
import java.util.List;
import java.util.Objects;

public class NewsAnalyserThread extends Thread {

    private static final int MILLI_SECONDS = 1000;
    private static final int TIME_GAP_BETWEEN_PRINTS = 10;
    private static final int POSITIVE_NEWS_ITEMS_LIMIT = 3;

    protected Socket socket;
    List<News> positiveNews;

    public NewsAnalyserThread(Socket clientSocket, List<News> positiveNews) {
        this.socket = clientSocket;
        this.positiveNews = positiveNews;
    }

    public void run() {
        ObjectInputStream objectInputStream = null;
        try {
            // Receive input object from the client socket
            InputStream inputStream = socket.getInputStream();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            objectInputStream = new ObjectInputStream(bufferedInputStream);
        } catch (IOException e) {
            System.out.println("Exception = " + e.getMessage());
        }

        long previousPrintTime = System.currentTimeMillis() / MILLI_SECONDS;
        NewsService newsService = new PositiveNewsService();

        News newsDTO = null;
        while (true) {
            try {
                newsDTO = (News) Objects.requireNonNull(objectInputStream).readObject();
            } catch (IOException | ClassNotFoundException e) {
                System.out.println("Exception = " + e.getMessage());
            }
            boolean isPositiveNews = newsService.isPositiveNews(newsDTO);
            if (isPositiveNews) {
                positiveNews.add(newsDTO);
            }
            //printEntireListOfPositiveNews();
            long currentTimeInSeconds = Instant.now().getEpochSecond();
            if (currentTimeInSeconds >= previousPrintTime + TIME_GAP_BETWEEN_PRINTS &&
                    positiveNews.size() >= POSITIVE_NEWS_ITEMS_LIMIT) {
                previousPrintTime = currentTimeInSeconds;
                newsService.printNews(positiveNews);
                positiveNews.clear();
            }
        }
    }

    private void printEntireListOfPositiveNews() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("\n");
        positiveNews.forEach(news -> System.out.println(news.getHeadLine()));
    }
}
