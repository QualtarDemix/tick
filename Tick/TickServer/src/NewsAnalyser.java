import dto.News;
import services.NewsService;
import services.impl.PositiveNewsService;

import java.net.*;
import java.io.*;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class NewsAnalyser {

    private static final int PORT = 5000;

    public static void main(String[] args) throws IOException {
        // Start server and wait for a connection
        ServerSocket server = new ServerSocket(PORT);
        System.out.println("Server started");
        System.out.println("Waiting for a client ...");
        CopyOnWriteArrayList<News> syncList = new CopyOnWriteArrayList<>();

        while (true) {
            Socket socket = server.accept();
            System.out.println("Client accepted");
            new NewsAnalyserThread(socket, syncList).start();
        }
    }
}
