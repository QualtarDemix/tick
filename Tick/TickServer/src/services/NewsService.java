package services;

import dto.News;

import java.util.List;

public interface NewsService {

    void printNews(List<News> news);

    boolean isPositiveNews(News newsDTO);
}
