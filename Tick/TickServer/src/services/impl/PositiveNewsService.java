package services.impl;

import dto.News;
import services.NewsService;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;
import java.util.function.Predicate;

public class PositiveNewsService implements NewsService {

    private static final List<String> POSITIVE_WORDS = Arrays.asList("up", "rise", "good", "success", "high", "über");
    private static final int POSITIVE_NEWS_ITEMS_LIMIT = 3;
    private static final String DELIMITER = " ";

    @Override
    public void printNews(List<News> positiveNews) {
        System.out.println("\nNumber of positive news items = "+positiveNews.size());
        positiveNews.sort(Comparator.comparingInt(News::getPriority));
        System.out.println("Top "+POSITIVE_NEWS_ITEMS_LIMIT+" positive news items : ");
        positiveNews.stream()
                .filter(distinctByHeadline(News::getHeadLine))
                .limit(POSITIVE_NEWS_ITEMS_LIMIT)
                .forEach(newsDTO -> System.out.println(newsDTO.getHeadLine()));
    }

    @Override
    public boolean isPositiveNews(News newsDTO) {
        String headLine = newsDTO.getHeadLine();
        String[] headlineArray = headLine.split(DELIMITER);
        List<String> wordsInHeadline = new ArrayList<>(Arrays.asList(headlineArray));
        int numberOfWords = wordsInHeadline.size();
        wordsInHeadline.retainAll(POSITIVE_WORDS);
        int numberOfCommonWords = wordsInHeadline.size();
        return numberOfCommonWords > (numberOfWords/2);
    }

    public static <News> Predicate<News> distinctByHeadline(Function<? super News, ?> headlineExtractor) {
        Set<Object> uniqueKeySet = ConcurrentHashMap.newKeySet();
        return newsDTO -> uniqueKeySet.add(headlineExtractor.apply(newsDTO));
    }
}
