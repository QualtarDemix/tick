package services.impl;

import dto.News;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import services.NewsService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PositiveNewsServiceTest {

    private static final String SAMPLE_NEGATIVE_HEADLINE = "Sample headline";
    private static final String SAMPLE_PARTIALLY_NEGATIVE_HEADLINE = "Sample partially negative headline up rise";
    private static final String SAMPLE_POSITIVE_HEADLINE = "up rise good";
    private static final Integer SAMPLE_PRIORITY = 4;

    NewsService newsService;

    @Before
    public void setUp() {
        newsService = new PositiveNewsService();
    }

    @Test
    public void printNews() {
        List<News> newsList = new ArrayList<>();

        newsService.printNews(newsList);
    }

    @Test
    public void returnFalseForCompletelyNegativeHeadline() {
        News newsDTO = new News(SAMPLE_NEGATIVE_HEADLINE, SAMPLE_PRIORITY);
        boolean actualResult = newsService.isPositiveNews(newsDTO);

        Assert.assertFalse(actualResult);
    }

    @Test
    public void returnFalseForPartiallyNegativeHeadline() {
        News newsDTO = new News(SAMPLE_PARTIALLY_NEGATIVE_HEADLINE, SAMPLE_PRIORITY);
        boolean actualResult = newsService.isPositiveNews(newsDTO);

        Assert.assertFalse(actualResult);
    }

    @Test
    public void returnTrueForPositiveHeadline() {
        News newsDTO = new News(SAMPLE_POSITIVE_HEADLINE, SAMPLE_PRIORITY);
        boolean actualResult = newsService.isPositiveNews(newsDTO);

        Assert.assertTrue(actualResult);
    }
}